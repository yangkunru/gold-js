/**
 * This function turns the specified string into camel cased identifier. Camel case (stylized
 * as camelCase; also known as camel caps or more formally as medial capitals) is the practice
 * of writing phrases such that each word or abbreviation in the middle of the phrase begins
 * with a capital letter, with no intervening spaces or punctuation. For example.
 *
 * - `safe hTML` -> `safeHtml`
 * - `escape HTML entities` -> `escapeHtmlEntities`
 *
 * The identifier should only contains english letters (`A` to `Z`, including upper and
 * lower case), digits (`0` to `9`) and underscore (`_`). Other characters will be treated
 * as delimiters. For example.
 *
 * - `safe+html` -> `safeHtml`
 *
 * @param {String} string The input string.
 */
export default function camelCase (string) {
  // TODO:
  //   Please implement the function.
  // <-start-
  if (!string) {
    return string;
  }
  var camelCaseString = '';
  var wordsArray = string.split(/\W/);
  console.log(wordsArray);
  for (let i = 0; i < wordsArray.length; i++) {
    if (i === 0) {
      camelCaseString += wordsArray[i].toLowerCase();
      continue;
    }
    camelCaseString += wordsArray[i].charAt(0).toUpperCase() + wordsArray[i].slice(1).toLowerCase();
  }
  return camelCaseString;
  // --end-->
}

// TODO:
//   You can add additional code here if you want
// <-start-

// --end-->
